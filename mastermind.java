/**
 * Author: John-Francis Mahama
 * Klasse: 3H
 */

package mastermind;

//Imports
import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;
import java.util.Objects;
import java.util.Collections;

public class Mastermind {
	public static void main(String[] args) throws IOException {
		System.out.println("Spiel gestartet. Anzahl Versuche : 5");
		
		// First variable definitions
		int tries = 5;	
		ArrayList<String> colors = new ArrayList<>();
		colors.add("r");
		colors.add("g");
		colors.add("b");
		colors.add("y");
		colors.add("s");
		colors.add("w");
		ArrayList<String> solution_code = new ArrayList<>();
		solution_code = generate_code(solution_code, colors);
		
		
		// Begin Game
		for(int i=tries;i>=0;i--)
		{
			boolean correct_status = processcode(colors, solution_code);
			// tries over
			if (i==0) {
				System.out.println("Alle versuche benutzt! Du hast den code nicht gefunden! Code: " + solution_code);
				break;
			}
			if (correct_status == true) {
				// Code found
				break;
			}
		}

	
	}
	
	static ArrayList<String> generate_code(ArrayList<String> color_code, ArrayList<String> colors){
		Random rand = new Random();
		for(int i = 0; i <= 3; i++){
			color_code.add(colors.get(rand.nextInt(colors.size() - 1)));
		}
		//System.out.println("FOR TESTING! CODE: " + color_code);
		return color_code;
	}
	
	static boolean processcode(ArrayList<String> colors, ArrayList<String> solution_code) throws IOException {
		String code = getcode();
		boolean correct_status = checkcode(colors, code, solution_code);
		return correct_status;
	}

	
	static boolean checkcode(ArrayList<String> colors, String code, ArrayList<String> solution_code) throws IOException{
		// check letters in code
				boolean correct_status = false;
				String status = "goodcode";
				String[] c = code.split("");
				for(int i=0;i<code.length();i++){
					if(colors.contains(c[i])) {
					}
					else {
						status = "badcode";
						break;
						}
				}			
				// Check if the code is valid
				if (status == "goodcode") {
					correct_status = check_code(solution_code, code);
					if (correct_status == true) {
						System.out.println("Gratuliere, du hast den Code gefunden! - Code: " + code);
						
					}
				}
					
				else {
					System.out.println("Ung�ltig Code! Bitte schreiben Sie kein kommas usw.");
					processcode(colors, solution_code);
				}
				return correct_status;
	}
	
	static boolean check_code(ArrayList<String> solution_code, String code){

		boolean found_code = false;
		ArrayList<String> code_list = new ArrayList<String>(Arrays.asList(code.split("")));
		int wrong_place = 0;
		int right_place = 0;
		ArrayList<String> handled = new ArrayList<String>();
		ArrayList<String> processed = new ArrayList<String>();
		ArrayList<String> skipped = new ArrayList<String>();
		//solution_code iterate
		outerloop:
		for(int i = 0; i <= 3; i++){
			// User code iterate
			for(int j = 0; j <= 3; j++){
				if(solution_code.contains(code_list.get(j))){
					// if color code in correct position
					if(i == j && Objects.equals(solution_code.get(i), code_list.get(j))){
						right_place += 1;
						processed.add(code_list.get(j));
						if (handled.contains(code_list.get(j)) && wrong_place > 0) {
							wrong_place -= 1;
						}
						continue outerloop;
					}
					else{
						int occurrences_solution = Collections.frequency(solution_code, code_list.get(j)); // letter in solution
							// more than 1 occurrence of letter in solution code
							if (occurrences_solution >= 1 && !handled.contains(code_list.get(j))) {
								handled.add(code_list.get(j));
								// Add to wrong place
								if (processed.contains(code_list.get(j))){
									int processed_occurances = Collections.frequency(processed, code_list.get(j)); // letter that is correct
									int skipped_occurances = Collections.frequency(skipped, solution_code.get(i)); // skipped letter
									int handled_occurances = Collections.frequency(handled, solution_code.get(i)); // letter that is wrong 
									wrong_place += (occurrences_solution - skipped_occurances - processed_occurances - handled_occurances);
								}
								else {
									wrong_place += occurrences_solution;
								}
							}
					}
				}
				else {
					skipped.add(solution_code.get(i));
					handled.add(solution_code.get(i));
					continue;
				}
			}
		}
		if(right_place == 4){
			found_code = true;
		}
		System.out.println("Falscher Ort: " + String.valueOf(wrong_place) + " Richtig: " + String.valueOf(right_place));
		return found_code;
	}
	
	static String getcode() throws IOException {
		// Read from console reader
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Geben Sie einen Versuchscode mit vier Buchstaben aus der Menge (r, g, b, y, s, w) ein:");
		String code = br.readLine();
		return code;
		}
}
